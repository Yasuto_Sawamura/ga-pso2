# -*- coding:utf-8 -*-

from pyevolve import G1DList,Selectors
from pyevolve import GSimpleGA,Initializators,Mutators, Consts
import random
import math

grt=100#世代
kts=2000#個体数

cdef double prob[10][5]
cdef int costs[10]

def read_prob(f_name):#対象のレアリティの強化成功率をテキストから読み込む
	fp=open(f_name,"r")
	ar=[]
	
	for line in fp:
		sar=[]
		s_line=line.strip().split()
		
		for datas in s_line:
			sar.append(float(datas))
		
		ar.append(sar)
	
	for i in range(10):
		for j in range(5):
			prob[i][j]=ar[i][j]
	

def cal_cost(g20):#評価関数(一回分)
	cdef int genoms[20]
	cdef double total_c=0
	cdef int kkt=0
	cdef int spent_item[3][5]
	cdef float s_or_f
	cdef int result_d
	
	total_c=0
	
	for i in xrange(3):
		for j in xrange(5):
			spent_item[i][j]=0
	
	for i in xrange(20):
		genoms[i]=g20[i]
	
	"""
	[0][1]は成功率+5%、[0][2]は成功率+10%、・・・
	[1][1]はリスク-1、・・・
	[2][0]は強化回数、[2][1]はグラインダー数
	[0][0],[1][0],[2][2]~[2][4]は空
	"""
	
	while kkt<10:
		s_or_f=random.random()
		s_or_f+=re_suc(genoms[kkt])
		
		if s_or_f>=prob[kkt][1]+prob[kkt][2]+prob[kkt][3]+prob[kkt][4]:#成功
			result_d=1
		elif s_or_f>=prob[kkt][2]+prob[kkt][3]+prob[kkt][4]:#失敗(-0)
			result_d=0
		elif s_or_f>=prob[kkt][3]+prob[kkt][4]:#失敗(-1)
			result_d=-1
		elif s_or_f>=prob[kkt][4]:#失敗(-2)
			result_d=-2
		else:#失敗(-3)
			result_d=-3
		
		result_d=final_result(result_d,genoms[kkt+10])#軽減アイテムを使用
		
		spent_item[2][0]+=1
		spent_item[2][1]+=grainder_num(11)
		spent_item[0][genoms[kkt]]+=1
		spent_item[1][genoms[kkt+10]]+=1
		kkt+=result_d
	
	total_c+=spent_item[2][1]*costs[0]#grinder
	total_c+=spent_item[2][0]*costs[1]#strenghening
	total_c+=spent_item[0][1]*costs[2]#seikouritu+5%
	total_c+=spent_item[0][2]*costs[3]#+10%
	total_c+=spent_item[0][3]*costs[4]#+20%
	total_c+=spent_item[1][1]*costs[5]#risk-1
	total_c+=spent_item[1][3]*costs[6]#risk-x
	
	return total_c,spent_item[0][4],spent_item[1][2]

def cal_fitness20(gp20):#評価関数。上を複数回数繰り返し、各項目の合計を取る
	cost3=[0,0,0]#(平均値とっても合計とっても適応度の大小は同じなので）
	cdef int g20[20]
	
	for i in xrange(20):
		g20[i]=gp20[i]
		
	
	for p in xrange(25):
		q=cal_cost(g20)
		cost3[0]+=q[0]
		cost3[1]+=q[1]
		cost3[2]+=q[2]
	
	cost3[0]/=25.0
	cost3[1]/=25.0
	cost3[2]/=25.0
	
	return 10000000.0/(cost3[0]*risk2_funk(cost3[2])+cube_funk(cost3[1]))

def cal_fitness20_test(g20):#テスト用関数
	cost3=[0,0,0]
	
	for p in xrange(25):
		q=cal_cost(g20)
		cost3[0]+=q[0]
		cost3[1]+=q[1]
		cost3[2]+=q[2]
	
	cost3[0]/=25.0
	cost3[1]/=25.0
	cost3[2]/=25.0
	return cost3

def re_suc(d): #強化成功率アップアイテムを%に変換
	if d==0:
		return 0
	elif d==1:
		return 0.05
	elif d==2:
		return 0.1
	elif d==3:
		return 0.2
	elif d==4:
		return 0.3
	else:
		print "error_suc"

def final_result(k,l):#強化リスク軽減を判定
	
	if k>=0:#強化に成功or失敗したが強化値が減らない場合
		return k
	else:#強化値が下がる場合
		if l==1:#リスク-1を使用
			return k+1
		elif l==2:#リスク-2を使用
			if k==-1:
				return 0
			else:
				return k+2
		elif l==3:
			return 0
		elif l==0 or l==4:
			return k
		else:
			print "error_result"

def grainder_num(ra):#レアリティから消費グラインダー数に変換。公開時に必要
	if ra==12:
		return 5
	elif ra==11:
		return 4
	elif ra==10:
		return 2
	else:
		return 1

def risk2_funk(num):#配布のみで手に入るアイテムのコスト計算
	if num==0:
		return math.exp(1)
	else:
		if num<costs[7]*3:
			return math.exp(1)
		else:
			return math.exp(((num*1.0)/(costs[7]*3+0.5)))

def cube_funk(num):
	"""if num==0:
		return math.exp(1)
	else:
		if num<costs[8]*5:
			return math.exp(1)
		else:
			return math.exp(((num*1.0)/(costs[8]*5+1)))
	"""
	if costs[8]!=0:
		return num*(2/costs[8])*150000
	elif num!=0:
		return 10000000
	else:
		return 0

def test_input(input_costs):
	
	for p in input_costs:
		if isinstance(p,str)==False:
			return 3
		if p.isdigit()==False:
			return 1
	
	if input_costs[1]=="":
		return 2
	
	for p in range(10):
		costs[p]=int(input_costs[p])
	return 0

def main(input_costs):
	
	corect=test_input(input_costs)
	if corect==1:
		return "Use only hankaku number"
	elif corect==2:
		return "Input Strenghening cost!"
	elif corect==3:
		return "starting"
	
	random.seed(34515)
	f_name=str(costs[9])+".txt"
	read_prob(f_name)
	
	g=G1DList.G1DList(20)#遺伝子の長さ
	g.setParams(rangemin=0,rangemax=4)#遺伝子の取る値の範囲
	g.evaluator.set(cal_fitness20)#適応度を計算する関数を指定
	#g.initializator.set(Initializators.G1DListInitializatorInteger)
	#g.mutator.set(Mutators.G1DListMutatorIntegerGaussian)

	ga=GSimpleGA.GSimpleGA(g)
	
	ga.setGenerations(grt)#世代数
	ga.setPopulationSize=(kts)
	ga.setMutationRate(0.1)#突然変異率
	ga.selector.set(Selectors.GTournamentSelector)#トーナメント方式
	
	ga.evolve(freq_stats=0)#実行
	best=ga.bestIndividual()
	res=best.genomeList
	
	result_html=""
	total=cal_fitness20_test(res)
	
	result_html+="spent meseta:"+str(total[0])+"<br>"
	result_html+="spent cube:"+str(total[1])+"<br>"
	result_html+="spent risk(-2):"+str(total[2])+"<br>"
	
	for p in range(10):
		result_html+=str(p)+"-"+str(p+1)+":\t\t"+generate_html(res[p],res[p+10])
	
	return result_html

def generate_html(a,b):
	res1=""
	res2=""
	
	if a==0:
		res1="none"
	elif a==1:
		res1="+5%"
	elif a==2:
		res1="+10%"
	elif a==3:
		res1="+20%"
	elif a==4:
		res1="+30%"
	else:
		print a
	
	if b==0 or b==4:
		res2="none"
	elif b==1:
		res2="-1"
	elif b==2:
		res2="-2"
	elif b==3:
		res2="kanzen"
	else:
		print b
	
	return res1+" / "+res2+"<br>"
	