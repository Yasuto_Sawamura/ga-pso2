#! -*- coding: utf-8 -*-

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [
    Extension( "simu_h", ["simu_h.pyx"] ),
]

setup(
    name = "Sample calculate app",
    cmdclass = { "build_ext" : build_ext },
    ext_modules = ext_modules,
)
