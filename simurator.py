#! /usr/bin/python
# -*- coding:utf-8 -*-

print "Content-Type: text/html;charset=UTF-8"
html1="""
<html>
<head>
	<title>（旧武器）強化コスト最小化シミュ</title>
</head>

<body>
<h1>（旧武器）強化コスト最小化シミュレーター</h1>

<form action="simurator.py" method="post">
	<p>レアリティ<br>
	10:<input type="radio" name="rare" value="10" checked>
	11:<input type="radio" name="rare" value="11">
	12:<input type="radio" name="rare" value="12">
	
	<p>グラインダーの価格<br>
	0:<input type="radio" name="g_cost" value="0" checked>
	400:<input type="radio" name="g_cost" value="400">
	1050:<input type="radio" name="g_cost" value="1050">
	
	<p>強化費用:<input type="text" name="s_cost" />
	<p>強化成功率+5%の価格:<input type="text" name="p5_cost" value="15000">
	<p>強化成功率+10%の価格<input type="text" name="p10_cost" value="100000">
	<p>強化成功率+20%の価格<input type="text" name="p20_cost" value="1000000">
	<p>リスク軽減(-1)の価格:<input type="text" name="k1_cost" value="1050">
	<p>リスク軽減(完全)の価格:<input type="text" name="kx_cost" value="270000">
	
	<p>リスク軽減の使用量<br>
	不使用:<input type="radio" name="k2_cost" value="0" checked>
	ちょっと:<input type="radio" name="k2_cost" value="1">
	多め:<input type="radio" name="k2_cost" value="2">
	
	<p>強化成功率+30%の使用量<br>
	不使用:<input type="radio" name="p30_cost" value="0" checked>
	ちょっと:<input type="radio" name="p30_cost" value="1">
	多め:<input type="radio" name="p30_cost" value="2">
	
	<p><input type="submit">
</form>
"""

import simu_h
import cgi
costs=[]
p=cgi.FieldStorage()

costs.append(p.getvalue("g_cost"))#0
costs.append(p.getvalue("s_cost"))#1
costs.append(p.getvalue("p5_cost"))#2
costs.append(p.getvalue("p10_cost"))#3
costs.append(p.getvalue("p20_cost"))#4
costs.append(p.getvalue("k1_cost"))#5
costs.append(p.getvalue("kx_cost"))#6
costs.append(p.getvalue("k2_cost"))#7
costs.append(p.getvalue("p30_cost"))#8
costs.append(p.getvalue("rare"))#9

results=simu_h.main(costs)

print html1
print results
print "</body>"
print "</html>"
print